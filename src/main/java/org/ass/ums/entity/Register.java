package org.ass.ums.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
@Entity
@Table(name="register_info")
public class Register implements Serializable {
	
	@Id
	@GenericGenerator(name="reg_auto",strategy="increment")
	@GeneratedValue(generator="reg_auto")
    @Column(name="alt_key")	
	private int altKey;
    
	@Column(name="first_name")
	private String firstName;
	
	@Column(name="email")
	private String email;
	
	@Column(name="city")
	private String city;
	
	@Column(name="contact_number")
    private String contactNumber;
	
	@Column(name="pin_code")
    private String pincode;

	public int getAltKey() {
		return altKey;
	}

	public void setAltKey(int altKey) {
		this.altKey = altKey;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	@Override
	public String toString() {
		return "Register [altKey=" + altKey + ", firstName=" + firstName + ", email=" + email + ", city=" + city
				+ ", contactNumber=" + contactNumber + ", pincode=" + pincode + "]";
	}

}
