package org.ass.ums.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="application_info")
public class Application implements Serializable{
@Id
@Column(name="app_id")
private int id;
@Column(name="app_name")
private String appName;
@Column(name="app_url")
private String url;
@Column(name="version_number")
private String versionNumber;
@Column(name="app_type")
private String type;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getAppName() {
	return appName;
}
public void setAppName(String appName) {
	this.appName = appName;
}
public String getUrl() {
	return url;
}
public void setUrl(String url) {
	this.url = url;
}
public String getVersionNumber() {
	return versionNumber;
}
public void setVersionNumber(String versionNumber) {
	this.versionNumber = versionNumber;
}
public String getType() {
	return type;
}
public void setType(String type) {
	this.type = type;
}



}
