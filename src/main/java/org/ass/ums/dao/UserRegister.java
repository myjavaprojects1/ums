package org.ass.ums.dao;

import org.ass.ums.dto.RegisterDto;
import org.ass.ums.entity.Register;

public interface UserRegister {
public void saveRegisterInfo(RegisterDto registerDto);
public Register getUserById(int altKey);
public void  updateRegisterInfo(int altKey,String city,String contactNumber);
}
