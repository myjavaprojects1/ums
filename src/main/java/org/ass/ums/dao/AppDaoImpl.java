package org.ass.ums.dao;

import org.ass.ums.dto.AppDto;
import org.ass.ums.entity.Application;
import org.ass.ums.util.ConnectionPropertiesUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class AppDaoImpl implements AppDao {

	@Override
	public void saveApp(AppDto appDto) {
		Application application=new Application();
		
		application.setAppName(appDto.getAppName());
		application.setId(100);
		application.setType(appDto.getType());
		application.setUrl(appDto.getUrl());
		application.setVersionNumber(appDto.getVersionNumber());
		
		 Configuration cfg = new Configuration();
		 cfg.setProperties(ConnectionPropertiesUtil.getConnectionProperties());
		 cfg.addAnnotatedClass(Application.class);
		 SessionFactory sessionFactory = cfg.buildSessionFactory();
		 Session session = sessionFactory.openSession();
		 Transaction transaction = session.beginTransaction();
		 session.save(application);
		 transaction.commit();
	}

}
