package org.ass.ums.dao;


import org.ass.ums.dto.RegisterDto;
import org.ass.ums.entity.Register;
import org.ass.ums.util.ConnectionPropertiesUtil;
import org.ass.ums.util.SessionFactoryObject;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class UserRegisterImpl implements UserRegister {
	
	@Override
	public void saveRegisterInfo(RegisterDto registerDto) {
		Register register=new Register();
		register.setCity(registerDto.getCity());
		register.setContactNumber(registerDto.getContactNumber());
		register.setEmail(registerDto.getEmail());
		register.setFirstName(registerDto.getFirstName());
		register.setPincode(registerDto.getPincode());		
				  
				
		SessionFactory sf=SessionFactoryObject.getSFObject();
		Session session=sf.openSession();
		Transaction transaction=session.beginTransaction();
		session.save(register);
		transaction.commit();
	}
	@Override
	
	public Register getUserById(int altKey) {
		SessionFactory sf=SessionFactoryObject.getSFObject();
		Session session=sf.openSession();
		Register register=session.get(Register.class,altKey);
		return register;
	}
	
	private void saveOrUpdateRegisterInfo(Register register) {
		SessionFactory sf=SessionFactoryObject.getSFObject();
		Session session=sf.openSession();
		Transaction transaction=session.beginTransaction();
		session.merge(register);
		transaction.commit();
	}
	

	@Override
	public void updateRegisterInfo(int altKey, String city, String contactNumber) {
		Register register=getUserById(altKey);
		if(register!=null) {
			register.setCity(city);
			register.setContactNumber(contactNumber);
			saveOrUpdateRegisterInfo(register);
		}
	}	
}
