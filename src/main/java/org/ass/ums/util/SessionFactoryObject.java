package org.ass.ums.util;

import org.ass.ums.entity.Register;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class SessionFactoryObject {
	public static SessionFactory getSFObject() {
		Configuration cfg=new Configuration();
		cfg.setProperties(ConnectionPropertiesUtil.getConnectionProperties());
		cfg.addAnnotatedClass(Register.class);
		SessionFactory sessionFactory=cfg.buildSessionFactory();
		return sessionFactory;
	}
}
